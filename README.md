chan-test
=========

Asterisk channel driver for testing purposes. To be installed on the XiVO PBX server.

Installing
==========

Install these utilities to enable compilation:

    apt-get install build-essential libssl-dev

To be able to install the _asterisk-dev_ package you must have the XiVO Solutions repository on your XiVO PBX. 
The package exists on xivo.solutions mirror but also on debian mirror. List all the versions:

    apt-cache policy asterisk-dev
    
Then use the name of the version from xivo.solutions:
    
    apt-get install asterisk-dev=(paste the version number)

Clone the source, build it and install:

    git clone git@gitlab.com:xivo.solutions/chan-test.git
    cd chan-test
    make
    service asterisk stop
    make install
    service asterisk start

Usage
=====

To create a new test channel from the asterisk CLI:

    test new <exten> <context> [cid_num] [cid_name]

To answer an (outbound) channel:

    test answer <channel_id_or_name>

The commands are also available via ARI (preferred way):

    curl -i -u 'xivo:Nasheow8Eag' -d '' 'http://127.0.0.1:5039/ari/chan_test/new?context=default&exten=1001'
    curl -i -u 'xivo:Nasheow8Eag' -d '' 'http://127.0.0.1:5039/ari/chan_test/answer?id=1469040639.7'

Test channels can also be dialed:

    Dial(Test/foo)
    Dial(Test/foo/autoanswer)
